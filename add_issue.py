import os
import requests
import json
import pandas as pd
import settings
 
BACKLOG_SPACE_URL = os.environ.get('BACKLOG_SPACE_URL')
BACKLOG_API_KEY = os.environ.get('BACKLOG_API_KEY')
BASE_URL = 'https://{space_url}.backlog.com/api/v2/{api_url}'
API_URL = 'issues'
PROJECT_ID = '2799'

INPUT_FILENAME = 'test.xlsx'

def add_issue(payload):
    url = BASE_URL.format(
        space_url=BACKLOG_SPACE_URL,
        api_url=API_URL,
    )
    params = {
        'apiKey': BACKLOG_API_KEY,
    }
    r = requests.post(url, params=params, data=payload)
    return r
 
if __name__ == '__main__':
    issue_type_id = '11981'
    priority_id = '3'

    df = pd.read_excel(, header=1)

    for index, row in df.iterrows():
        payload={
            'projectId': PROJECT_ID,
            'issueTypeId': issue_type_id,
            'priorityId': priority_id,
            'summary': row['summary'],
            'description': row['description'],
        }
        r = add_issue(payload=payload)
        response_obj = json.loads(r.text)
        print(r, response_obj['summary'])

import os
import requests
import json
import pandas as pd
import settings
 
BACKLOG_SPACE_URL = os.environ.get('BACKLOG_SPACE_URL')
BACKLOG_API_KEY = os.environ.get('BACKLOG_API_KEY')
BASE_URL = 'https://{space_url}.backlog.com/api/v2/{api_url}'
API_URL = 'users'
OUTPUT_FILENAME = 'users.csv'

if __name__ == '__main__':
    url = BASE_URL.format(
        space_url=BACKLOG_SPACE_URL,
        api_url=API_URL,
    )
    params = {
        'apiKey': BACKLOG_API_KEY,
    }
    r = requests.get(url, params=params)
    user_obj = json.loads(r.text)
    df = pd.DataFrame(user_obj)
    df.to_csv(OUTPUT_FILENAME)

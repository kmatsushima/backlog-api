import os
import requests
import json
import pandas as pd
import settings
 
BACKLOG_SPACE_URL = os.environ.get('BACKLOG_SPACE_URL')
BACKLOG_API_KEY = os.environ.get('BACKLOG_API_KEY')
BASE_URL = 'https://{space_url}.backlog.com/api/v2/{api_url}/{issue_id}'
API_URL = 'issues'
PROJECT_ID = '2799'
INPUT_FILENAME = 'patch.xlsx'

def patch_issue(issue_id, payload):
    url = BASE_URL.format(
        space_url=BACLLOG_SPACE_URL,
        api_url=BACKLOG_API_URL,
        issue_id=issue_id,
    )

    params = {
        'apiKey': API_KEY,
    }

    r = requests.patch(url, params=params, data=payload)
    return r
 
if __name__ == '__main__':
    df = pd.read_excel(INPUT_FILENAME, header=1)

    for index, row in df.iterrows():
        payload = {
            # 'issue_type_id': '11981',
            # 'priority_id': '3',
            'summary': row['summary'],
        }
        r = add_issue(issue_id=row['id'], payload=payload)
        response_obj = json.loads(r.text)
        print(r, response_obj['summary'])

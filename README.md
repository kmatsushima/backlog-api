# about
当社の取引先にて要望のあった、Backlogへのチケット一括登録のために作成したもの。

機能は以下。
- [x] ユーザーの取得
- [ ] ユーザーの登録
- [ ] ユーザーのプロジェクトへの追加
- [x] チケットの登録（エクセルファイル読み込み）
- [x] チケットの取得
- [x] チケットの更新（エクセルファイル読み込み）

カスタムフィールド等の対応は汎用化可不のため、ソースコード要修正。

エクセルファイルのテンプレートは暇な時に or 必要そうであったら作成予定。

# how to use
.envファイルの作成
- `.env.sample`ファイルを`.env`としてコピーして作成

venv作成
```bash
python -m venv backlog
```

venvアクティベート
```bash
source backlog/bin/activate
```

pythonライブラリインストール
```bash
pip install -r requirements.txt
```

各種コードの実行
```bash
python add_issue.py
```

venvディアクティベート
```bash
deactivate
```
